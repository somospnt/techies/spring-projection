package com.somospnt.demo.springprojection.projection;

import lombok.Value;

@Value
public class UsuarioPublico {

    private Long id;
    private String nombre;
    private String email;

}
