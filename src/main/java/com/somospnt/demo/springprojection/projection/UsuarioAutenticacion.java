package com.somospnt.demo.springprojection.projection;

public interface UsuarioAutenticacion {
    
    String getNombre();
    String getPassword();
    
}
