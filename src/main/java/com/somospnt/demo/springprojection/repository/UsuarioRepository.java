package com.somospnt.demo.springprojection.repository;

import com.somospnt.demo.springprojection.domain.Usuario;
import com.somospnt.demo.springprojection.projection.UsuarioPublico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    
    @Query("select new com.somospnt.demo.springprojection.projection.UsuarioPublico(id, nombre, email) from Usuario "
            + "where nombre = ?")
    UsuarioPublico buscarPorNombre(String nombre);
    

}
