package com.somospnt.demo.springprojection.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Usuario {

    @Id
    private Long id;
    private String nombre;
    private String password;
    private String email;

}
